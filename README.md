# Super **Simp**le Pytorch Type **Hint** (simp_hint)
![](https://i.pinimg.com/originals/b6/e5/c1/b6e5c185f1d0461d22d7684768be70fb.jpg)

This package provides a super simple way of type hinting annotating pytorch tensors according to their dtypes.

## What Problem Does It Solve?
![Type Hint Error](lfs_resources/type hint error.png)
For way too long, I have been annoyed of not being able to annotate an `torch.Tensor` initialized with LongTensor as a `LongTensor`.
This package provides a light weight method of annotating the LongTensor as it's own type.
You must instead do:
```python
from torch import Tensor, LongTensor

tensor: Tensor = LongTensor([1, 2, 3])
```

But this package allows you to explicitely annotate the dtype of the tensor.
```python
from simp_hint import LongTensorHint, long_tensor

tensor: LongTensorHint = long_tensor([1, 2, 3])

# Alternatively
from torch import LongTensor
tensor: LongTensorHint = LongTensorHint(LongTensor([1, 2, 3]))
```

## Installation
```
pip install simp-hint
```

## What Simp Hint Does **Not** Do:
* Provide runtime type check for tensor dtypes
* Provide type hint for tensor shapes
* Provide type hint for tensorflow tensors, jax array or numpy arrays

## What Simp Hint Does:
* Provide explicitly stated type hints for pytorch tensor based on dtypes with almost zero compute overhead

## Examples
Assigning LongTensor:
```python
from simp_hint import *
import torch
from torch import LongTensor, tensor

long_tensor_a: LongTensorHint = long_tensor([1, 2, 3])
long_tensor_b: LongTensorHint = LongTensorHint(LongTensor([1, 2, 3]))
long_tensor_c: LongTensorHint = LongTensorHint(tensor([1, 2, 3]), dtype=torch.long)
```
Assigning FloatTensor
```python
from simp_hint import float_tensor, FloatTensorHint

float_tensor: FloatTensorHint = float_tensor([1.1, 1.2, 1.3])
```
Assigning FloatTensor to LongTensorHint
![](lfs_resources/assigning_float_tensor_to_long_tensor.png)


## How It Works
Simp Hint defines a [NewType](https://docs.python.org/3/library/typing.html#newtype) for each dtypes.
E.g.
```python
from typing import NewType

LongTensorHint = NewType('LongTensorHint', torch.Tensor) 
```
When `long_tensor` is called, it creates a new `torch.Tensor` and wraps it with the LongTensorHint.
I.e.
```python
from typing import NewType
from torch import LongTensor

def long_tensor(*args, **kwargs)->LongTensorHint:
    return LongTensorHint(LongTensor(*args, **kwargs))
```

Since NewType does not create a new class (No longer true after python 3.10 but still as fast after python 3.11), it results is compute overhead of no more than a regular function call. It also provides additional benfit of allowing you to treat the newly created LongTensorHint just like you would treat a torch.Tensor.

I.e.
```python
type(LongTensor([1, 2, 3]))
>>> torch.Tensor
type(long_tensor([1, 2, 3]))
>>> torch.Tensor
```

## Subtle Issues

Becaue torch.LongTensor() returns a troch.Tensor type, unfortunately you can not assign LongTensorHint as LongTensor and vise versa.
(As what you are doing is assign torch.Tensor as torch.LongTensor.)

![](lfs_resources/assigning_long_tensor_to_long_tensor_hint.png)

To assign a `LongTensor` as a `LongTensorHint`, you can wrap it with the `NamedType`.
I.e.
```python
from simp_hint import LongTensorHint
from torch import LongTensor

tensor: LongTensorHint = LongTensorHint(LongTensor([1, 2, 3]))
```

However, be careful as there is nothing stopping you from wrapping a `FloatTensor` as a `LongTensorHint` since `LongTensorHint` is nothing more than a `NewType`. Wrapping a `FloatTensor` with `LongTensorHint` will not alter the data of `FloatTensor` what so ever.

E.g.
```python
from simp_hint import LongTensorHint
from torch import FloatTensor

tensor: LongTensorHint = LongTensorHint(FloatTensor([1.1, 2.2, 3.3]))

print(tensor)
>>> tensor([1.1000, 2.2000, 3.3000])
```

# Notes
* The interface, implementation etc are subject to change, please do not use it in a multi billion dollar project
* If you want a richer, more sophisticated type annotation package with runtime check etc, I would suggest you check out the [jaxtyping](https://github.com/patrick-kidger/jaxtyping) project
* I would be interested in adding super simple runtime shape check etc, so if you have any idea, please let me know
* It looks like there are various progresses on official support for type hitning tensors, so this project will probably not serve a purpose when they are fully supported:
    * https://github.com/pytorch/pytorch/issues/98702
    * https://github.com/MPoL-dev/MPoL/issues/54